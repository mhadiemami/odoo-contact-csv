# Odoo contacts' Sample CSV 

Sample CSV files for importing contacts into odoo contact directory.

In general odoo contacts can be either company or individual in which can be set via **is_company** value.

To import contacts (companies and individuals) and map their relations as well, you need to forst import all companies and then import individuals.

For each individual you need to specify **related company / external ID** and **function** which are the company's ID and job role.

Keep in mind you can specify each company's ID in company's CSV file and easily put the same number in person's **related company / external ID**.
